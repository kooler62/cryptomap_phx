defmodule Cryptomap.Application do
  @moduledoc """
  The Cryptomap Application Service.

  The cryptomap system business domain lives in this application.

  Exposes API to clients such as the `CryptomapWeb` application
  for use in channels, controllers, and elsewhere.
  """
  use Application

  def start(_type, _args) do
    import Supervisor.Spec, warn: false

    Supervisor.start_link([
      supervisor(Cryptomap.Repo, []),
    ], strategy: :one_for_one, name: Cryptomap.Supervisor)
  end
end

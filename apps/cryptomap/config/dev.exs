use Mix.Config

# Configure your database
config :cryptomap, Cryptomap.Repo,
  adapter: Ecto.Adapters.Postgres,
  username: "postgres",
  password: "postgres",
  database: "cryptomap_dev",
  hostname: "localhost",
  pool_size: 10

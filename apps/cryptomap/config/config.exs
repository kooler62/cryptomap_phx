use Mix.Config

config :cryptomap, ecto_repos: [Cryptomap.Repo]

import_config "#{Mix.env}.exs"

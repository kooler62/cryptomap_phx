# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.
use Mix.Config

# General application configuration
config :cryptomap_web,
  namespace: CryptomapWeb,
  ecto_repos: [Cryptomap.Repo]

# Configures the endpoint
config :cryptomap_web, CryptomapWeb.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "8V5dOxsetJqrjdmdXyR/7R9+qIKoYmP6uHLvByHNF3v5srJU14aLI6UuNVhYiliu",
  render_errors: [view: CryptomapWeb.ErrorView, accepts: ~w(html json)],
  pubsub: [name: CryptomapWeb.PubSub,
           adapter: Phoenix.PubSub.PG2]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

config :cryptomap_web, :generators,
  context_app: :cryptomap

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env}.exs"

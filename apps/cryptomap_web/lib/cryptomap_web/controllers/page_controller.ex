defmodule CryptomapWeb.PageController do
  use CryptomapWeb, :controller

  def index(conn, _params) do
    render conn, "index.html"
  end
end
